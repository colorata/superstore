package com.colorata.superstore

import kotlin.reflect.KProperty
import kotlinx.serialization.Serializable

interface Preference<T, R> {
    val initial: @Serializable T
    val saver: (name: String) -> PreferenceSaver<T>

    operator fun getValue(thisObj: Any?, property: KProperty<*>): R

    companion object
}

interface MutablePreference<T, R> : Preference<T, R> {
    operator fun setValue(thisObj: Any?, property: KProperty<*>, value: T)
}
