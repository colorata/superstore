package com.colorata.superstore

internal enum class Platform {
    Android,
    Jvm
}

internal expect val platform: Platform
