package com.colorata.superstore

class NoPreferenceConfigException(className: String) :
    Exception(noPreferenceConfigMessage(className))

private fun noPreferenceConfigMessage(className: String): String {
    return if (platform == Platform.Android) {
        """
            Config must not be null!
            
            Check you defined it in onCreate like this:
            
            class MainActivity: ... {
                override fun onCreate(...) {
                    super.onCreate(...)
                    ${className}.config = androidSuperStoreConfig()
                }
            }
        """.trimIndent()
    } else {
        """
            Config must not be null!
            
            Check you defined it in main like this:
            
            fun main() {
                ${className}.config = superStoreConfig("/path/to/dir/")
            }
            
            Or like this:
            
            object $className : PreferencesScope(
                configuration = superStoreConfig("/path/to/dir")
            ) {
                ...
            }
        """.trimIndent()
    }
}
