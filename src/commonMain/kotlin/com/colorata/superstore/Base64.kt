package com.colorata.superstore

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import okio.ByteString.Companion.decodeBase64
import okio.ByteString.Companion.encodeUtf8

fun String.decodeFromBase64(): String = decodeBase64()?.utf8().orEmpty()

fun String.encodeToBase64(): String = encodeUtf8().base64Url()

inline fun <reified T> Preference.Companion.base64Saver(
    name: String,
    initial: T, noinline handler: () ->
    FileHandler
): PreferenceSaver<T> =
    preferenceSaver(
        save = { value ->
            val h = handler()
            val fileContent = h.contentsOrThrow()
            val encoded = Preference.encodeValueToBase64(fileContent, value, name)
            h.write(encoded)
        },
        restore = {
            val h = handler()
            val fileContent = handler().contentsOrThrow()
            if (h.containsPreference(name))
                Preference.decodeValueFromBase64(fileContent, name)
            else initial
        }
    )

inline fun <reified T> Preference.Companion.decodeValueFromBase64(
    contents: String,
    name: String
): T {
    val nameBase64Encoded = name.encodeToBase64()
    val lines = contents.split("\n")
    val lineWithValue = lines.find {
        it.substringBefore(":") ==
                nameBase64Encoded
    }

    requireNotNull(lineWithValue) {
        """
        No value in path!
        Encoded values:
        $contents
    """.trimIndent()
    }
    return Json.decodeFromString(lineWithValue.substringAfter(":").decodeFromBase64())
}

inline fun <reified T> Preference.Companion.encodeValueToBase64(
    contents: String,
    value: T,
    name: String
): String {
    val nameBase64Encoded = name.encodeToBase64()
    val lines = contents.split("\n").toMutableList()
    val lineWithValueIndex = lines.indexOfFirst {
        it.substringBefore(":") ==
                nameBase64Encoded
    }
    val encodedValue = Json.encodeToString(value).encodeToBase64()
    if (lineWithValueIndex == -1) {
        lines.add("$nameBase64Encoded:$encodedValue")
    } else {
        lines[lineWithValueIndex] =
            "$nameBase64Encoded:$encodedValue"
    }
    return lines.joinToString("\n")
}
