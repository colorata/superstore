package com.colorata.superstore

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import kotlin.reflect.KProperty
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.Serializable

inline fun <reified T> PreferencesScope.mutablePreferenceOf(
    initial: @Serializable T,
    noinline saver: (name: String) -> PreferenceSaver<T> =
        defaultSaver(initial)
): MutablePreference<T, T> = MutablePreferenceImpl(
    initial = initial,
    saver = saver
)

@Stable
class MutablePreferenceImpl<T>(
    override val initial: T,
    override val saver: (name: String) -> PreferenceSaver<T>
) : MutablePreference<T, T> {

    private lateinit var _value: MutableState<T>

    private var _saver: PreferenceSaver<T>? by mutableStateOf(null)
    override fun setValue(thisObj: Any?, property: KProperty<*>, value: T) {
        initVariablesIfNotSet(property, value)

        _value.value = value
        runBlocking {
            _saver!!.save(value)
        }
    }

    override fun getValue(thisObj: Any?, property: KProperty<*>): T {
        initVariablesIfNotSet(property)

        runBlocking {
            _value.value = _saver!!.restore()
        }
        return _value.value
    }

    private fun initVariablesIfNotSet(property: KProperty<*>, value: T = initial) {
        if (_saver == null) _saver = saver(property.name)
        if (!::_value.isInitialized) _value = mutableStateOf(value)
    }
}

fun FileHandler.containsPreference(name: String): Boolean {
    val nameBase64Encoded = name.encodeToBase64()
    val lines = contentsOrThrow().split("\n")
    val lineWithValue = lines.find {
        it.substringBefore(":") ==
                nameBase64Encoded
    }
    return lineWithValue != null
}

