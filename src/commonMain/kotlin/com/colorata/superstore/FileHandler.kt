package com.colorata.superstore

expect class FileHandler(path: String) {
    suspend fun updateLocalContents()

    suspend fun write(value: String)

    var contents: String?
        internal set
}

fun FileHandler.contentsOrThrow(): String {
    requireNotNull(contents) { "File contents is not initiated!" }
    return contents!!
}
