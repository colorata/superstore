package com.colorata.superstore

interface SuperStoreConfig {
    val pathToDir: String
}

class CommonSuperStoreConfig internal constructor(override val pathToDir: String) : SuperStoreConfig

fun superStoreConfig(pathToDir: String) = CommonSuperStoreConfig(pathToDir)
