package com.colorata.superstore

class AnonymousPreferenceClassException :
    Exception(anonymousClassMessage)

internal val anonymousClassMessage = """
    Preference was created in anonymous class and no name provided!
    
    Check you defined preferences like this(if you prefer anonymous):
    
    object : PreferencesScope(name = "preferences") {
        ...
    }
    """.trimIndent()
