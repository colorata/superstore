package com.colorata.superstore

import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import kotlin.reflect.KProperty
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.onStart
import kotlinx.serialization.Serializable

inline fun <reified T> PreferencesScope.flowPreferenceOf(
    initial: @Serializable T,
    noinline saver: (name: String) -> PreferenceSaver<T> =
        defaultSaver(initial)
): FlowPreference<T> = FlowPreference(
    initial = initial,
    saver = saver
)

@Stable
class FlowPreference<T>(
    override val initial: T,
    override val saver: (name: String) -> PreferenceSaver<T>
) : Preference<T, MutablePreferenceFlow<T>> {

    @Stable
    private val flow by lazy {
        MutablePreferenceFlow(initial)
    }
    override fun getValue(
        thisObj: Any?,
        property: KProperty<*>
    ): MutablePreferenceFlow<T> {
        if (flow.saver == null) flow.saver = saver(property.name)
        return flow
    }
}


@Stable
class MutablePreferenceFlow<T>(
    initial: T
) : Flow<T> {
    internal var saver: PreferenceSaver<T>? by mutableStateOf(null)

    @Stable
    private val _flow = MutableStateFlow(initial)
    override suspend fun collect(collector: FlowCollector<T>) {
        _flow.onStart {
            requireNotNull(saver) { "Preference Saver is not provided" }
            _flow.emit(saver!!.restore())
        }.collect(collector)
    }


    suspend fun emit(value: T) {
        _flow.emit(value)
        requireNotNull(saver) { "Preference Saver is not provided" }
        saver!!.save(value)
    }
}
