package com.colorata.superstore

interface PreferenceSaver<T> {
    suspend fun save(value: T)

    suspend fun restore(): T
}

fun <T> preferenceSaver(
    save: suspend PreferenceSaver<T>.(value: T) -> Unit,
    restore: suspend PreferenceSaver<T>.() -> T
) = object : PreferenceSaver<T> {

    override suspend fun save(value: T) {
        return save(this, value)
    }

    override suspend fun restore(): T {
        return restore(this)
    }
}
