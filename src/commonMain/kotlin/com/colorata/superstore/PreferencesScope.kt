package com.colorata.superstore

import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import kotlinx.coroutines.runBlocking

private const val DEFAULT_FILE_EXTENSION = ".txt"

@Stable
abstract class PreferencesScope(
    private val fileName: String? = null,
    configuration: SuperStoreConfig? = null
) {
    var config: SuperStoreConfig? by mutableStateOf(configuration)

    @Stable
    val handler by lazy {
        if (config == null) throw NoPreferenceConfigException(this::class.simpleName!!)
        if (fileName == null && this::class.qualifiedName == null) throw AnonymousPreferenceClassException()
        val actualFileName = fileName ?: (this::class.qualifiedName + DEFAULT_FILE_EXTENSION)
        val path = config!!.pathToDir + "/" + actualFileName

        FileHandler(path).apply {
            runBlocking {
                updateLocalContents()
            }
        }
    }
}

inline fun <reified T> PreferencesScope.defaultSaver(
    initial: T
) = { name: String -> Preference.base64Saver(name, initial) { handler } }
