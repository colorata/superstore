package com.colorata.superstore

import android.content.Context


class AndroidSuperStoreConfig(override val pathToDir: String) : SuperStoreConfig

fun Context.androidSuperStoreConfig() =
    AndroidSuperStoreConfig(
        filesDir.absolutePath
    )
