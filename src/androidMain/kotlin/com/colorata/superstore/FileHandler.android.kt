package com.colorata.superstore

import java.io.File
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

actual class FileHandler actual constructor(val path: String) {
    actual suspend fun updateLocalContents() {
        withContext(Dispatchers.IO) {
            val file = File(path)
            if (!file.exists()) {
                file.createNewFile()
            }
            val text = file.readText()
            contents = text
        }
    }

    actual suspend fun write(value: String) {
        withContext(Dispatchers.IO) {
            val file = File(path)
            if (!file.exists()) {
                file.createNewFile()
            }
            contents = value
            file.writeText(value)
        }
    }

    actual var contents: String? = null
        internal set
}

