import java.io.File

fun Any.resource(name: String) = File(javaClass.getResource(name)!!.path)

