import com.colorata.superstore.PreferencesScope
import com.colorata.superstore.superStoreConfig
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest

abstract class TestScope {
    val resource by lazy { resource(resourceName) }

    @OptIn(ExperimentalCoroutinesApi::class)
    fun <T : PreferencesScope> T.test(
        initialPrefs: String = "",
        block: suspend T.() -> Unit
    ) {
        runTest {
            if (config == null) config = superStoreConfig(resource.parent)
            resource.writeText(initialPrefs)
            block(this@test)
        }
    }

    abstract fun `Flow Single Preferences Test`()
    abstract fun `Flow Multi Preferences Test`()
    abstract fun `Flow Serializable Preferences Test`()
    abstract fun `Flow Nullable Serializable Preferences Test`()

    abstract fun `Mutable Single Preferences Test`()
    abstract fun `Mutable Multi Preferences Test`()
    abstract fun `Mutable Serializable Preferences Test`()
    abstract fun `Mutable Nullable Serializable Preferences Test`()


    companion object {
        const val resourceName = "Prefs.txt"
    }
}
