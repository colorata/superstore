@file:Suppress("ClassName")

import com.colorata.superstore.PreferencesScope
import com.colorata.superstore.flowPreferenceOf
import com.colorata.superstore.mutablePreferenceOf
import kotlinx.serialization.Serializable

@Serializable
data class User(
    val name: String,
    val surname: String
)

object `Flow Single Preferences` : PreferencesScope(TestScope.resourceName) {
    val counter by flowPreferenceOf(Defaults.counter)

    object Defaults {
        const val counter = 0

        val storageValue = """
            
            Y291bnRlcg==:OTk=
        """.trimIndent()

        const val counterStorage = 99
    }
}

object `Flow Multi Preferences` : PreferencesScope(TestScope.resourceName) {
    val counter1 by flowPreferenceOf(Defaults.counter1)

    val counter2 by flowPreferenceOf(Defaults.counter2)

    object Defaults {
        const val counter1 = 0
        const val counter2 = 100

        val storageValue = """
            
            Y291bnRlcjE=:OQ==
            Y291bnRlcjI=:MTAwMA==
        """.trimIndent()

        const val counter1Storage = 9
        const val counter2Storage = 1000
    }
}

object `Flow Serializable Preferences` : PreferencesScope(TestScope.resourceName) {
    val user by flowPreferenceOf(Defaults.user)

    object Defaults {
        val user = User("", "")

        val storageValue = """
            
            dXNlcg==:eyJuYW1lIjoiQSIsInN1cm5hbWUiOiJCIn0=
        """.trimIndent()

        val userStorage = User("A", "B")
    }
}

object `Flow Nullable Serializable Preferences` : PreferencesScope(TestScope.resourceName) {
    val user by flowPreferenceOf(Defaults.user)

    object Defaults {
        val user: User? = null

        val storageValue = """
            
            dXNlcg==:bnVsbA==
        """.trimIndent()

        val userStorage: User? = null
    }
}

object `Mutable Single Preferences` : PreferencesScope(TestScope.resourceName) {
    var counter by mutablePreferenceOf(Defaults.counter)

    object Defaults {
        const val counter = 0

        val storageValue = """
            
            Y291bnRlcg==:OTk=
        """.trimIndent()

        const val counterStorage = 99
    }
}

object `Mutable Multi Preferences` : PreferencesScope(TestScope.resourceName) {
    var counter1 by mutablePreferenceOf(Defaults.counter1)

    var counter2 by mutablePreferenceOf(Defaults.counter2)

    object Defaults {
        const val counter1 = 0
        const val counter2 = 100

        val storageValue = """
            
            Y291bnRlcjE=:OQ==
            Y291bnRlcjI=:MTAwMA==
        """.trimIndent()

        const val counter1Storage = 9
        const val counter2Storage = 1000
    }
}

object `Mutable Serializable Preferences` : PreferencesScope(TestScope.resourceName) {
    var user by mutablePreferenceOf(Defaults.user)

    object Defaults {
        val user = User("", "")

        val storageValue = """
            
            dXNlcg==:eyJuYW1lIjoiQSIsInN1cm5hbWUiOiJCIn0=
        """.trimIndent()

        val userStorage = User("A", "B")
    }
}

object `Mutable Nullable Serializable Preferences` : PreferencesScope(TestScope.resourceName) {
    var user by mutablePreferenceOf(Defaults.user)

    object Defaults {
        val user: User? = null

        val storageValue = """
            
            dXNlcg==:bnVsbA==
        """.trimIndent()

        val userStorage: User? = null
    }
}
