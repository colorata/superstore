import kotlin.test.assertEquals
import kotlinx.coroutines.flow.first
import org.junit.jupiter.api.Test

class NonEmptyStorageTest : TestScope() {
    @Test
    override fun `Flow Single Preferences Test`() {
        `Flow Single Preferences`.test(`Flow Single Preferences`.Defaults.storageValue) {
            assertEquals(`Flow Single Preferences`.Defaults.counterStorage, counter.first())
            counter.emit(15)
            assertEquals(15, counter.first())
        }
    }

    @Test
    override fun `Flow Multi Preferences Test`() {
        `Flow Multi Preferences`.test(`Flow Multi Preferences`.Defaults.storageValue) {
            assertEquals(`Flow Multi Preferences`.Defaults.counter1Storage, counter1.first())
            assertEquals(`Flow Multi Preferences`.Defaults.counter2Storage, counter2.first())

            counter1.emit(919)
            counter2.emit(1095)

            assertEquals(919, counter1.first())
            assertEquals(1095, counter2.first())
        }
    }

    @Test
    override fun `Flow Serializable Preferences Test`() {
        `Flow Serializable Preferences`.test(`Flow Serializable Preferences`.Defaults.storageValue) {
            assertEquals(`Flow Serializable Preferences`.Defaults.userStorage, user.first())

            user.emit(User("AA", "BA"))

            assertEquals(User("AA", "BA"), user.first())
        }
    }

    @Test
    override fun `Flow Nullable Serializable Preferences Test`() {
        `Flow Nullable Serializable Preferences`.test(
            `Flow Nullable Serializable Preferences`
                .Defaults.storageValue
        ) {
            assertEquals(
                `Flow Nullable Serializable Preferences`.Defaults.userStorage,
                user.first()
            )

            user.emit(User("AА", "B"))

            assertEquals(User("AА", "B"), user.first())
        }
    }

    @Test
    override fun `Mutable Single Preferences Test`() {
        `Mutable Single Preferences`.test(`Mutable Single Preferences`.Defaults.storageValue) {
            assertEquals(`Mutable Single Preferences`.Defaults.counterStorage, counter)

            counter = 99

            assertEquals(99, counter)
        }
    }

    @Test
    override fun `Mutable Multi Preferences Test`() {
        `Mutable Multi Preferences`.test(`Mutable Multi Preferences`.Defaults.storageValue) {
            assertEquals(`Mutable Multi Preferences`.Defaults.counter1Storage, counter1)
            assertEquals(`Mutable Multi Preferences`.Defaults.counter2Storage, counter2)

            counter1 = 919
            counter2 = 1095

            assertEquals(919, counter1)
            assertEquals(1095, counter2)
        }
    }

    @Test
    override fun `Mutable Serializable Preferences Test`() {
        `Mutable Serializable Preferences`.test(`Mutable Serializable Preferences`.Defaults.storageValue) {
            assertEquals(`Mutable Serializable Preferences`.Defaults.userStorage, user)

            user = User("AA", "BA")

            assertEquals(User("AA", "BA"), user)
        }
    }

    @Test
    override fun `Mutable Nullable Serializable Preferences Test`() {
        `Mutable Nullable Serializable Preferences`.test(
            `Mutable Nullable Serializable Preferences`.Defaults.storageValue
        ) {
            assertEquals(`Mutable Nullable Serializable Preferences`.Defaults.userStorage, user)

            user = User("AA", "BA")

            assertEquals(User("AA", "BA"), user)
        }
    }
}
