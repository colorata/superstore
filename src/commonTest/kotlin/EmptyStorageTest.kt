import kotlin.test.assertEquals
import kotlinx.coroutines.flow.first
import org.junit.jupiter.api.Test

class EmptyStorageTest : TestScope() {

    @Test
    override fun `Flow Single Preferences Test`() {
        `Flow Single Preferences`.test {
            assertEquals(`Flow Single Preferences`.Defaults.counter, counter.first())
            counter.emit(99)
            assertEquals(99, counter.first())
        }
    }

    @Test
    override fun `Flow Multi Preferences Test`() {
        `Flow Multi Preferences`.test {
            assertEquals(`Flow Multi Preferences`.Defaults.counter1, counter1.first())
            assertEquals(`Flow Multi Preferences`.Defaults.counter2, counter2.first())

            counter1.emit(9)
            counter2.emit(1000)

            assertEquals(9, counter1.first())
            assertEquals(1000, counter2.first())
        }
    }

    @Test
    override fun `Flow Serializable Preferences Test`() {
        `Flow Serializable Preferences`.test {
            assertEquals(`Flow Serializable Preferences`.Defaults.user, user.first())

            user.emit(User("A", "B"))

            assertEquals(User("A", "B"), user.first())
        }
    }

    @Test
    override fun `Flow Nullable Serializable Preferences Test`() {
        `Flow Nullable Serializable Preferences`.test {
            assertEquals(`Flow Nullable Serializable Preferences`.Defaults.user, user.first())

            user.emit(User("A", "B"))

            assertEquals(User("A", "B"), user.first())
        }
    }

    @Test
    override fun `Mutable Single Preferences Test`() {
        `Mutable Single Preferences`.test {
            assertEquals(`Mutable Single Preferences`.Defaults.counter, counter)

            counter = 99

            assertEquals(99, counter)
        }
    }

    @Test
    override fun `Mutable Multi Preferences Test`() {
        `Mutable Multi Preferences`.test {
            assertEquals(`Mutable Multi Preferences`.Defaults.counter1, counter1)
            assertEquals(`Mutable Multi Preferences`.Defaults.counter2, counter2)

            counter1 = 9
            counter2 = 1000

            assertEquals(9, counter1)
            assertEquals(1000, counter2)
        }
    }

    @Test
    override fun `Mutable Serializable Preferences Test`() {
        `Mutable Serializable Preferences`.test {
            assertEquals(`Mutable Serializable Preferences`.Defaults.user, user)

            user = User("A", "B")

            assertEquals(User("A", "B"), user)
        }
    }

    @Test
    override fun `Mutable Nullable Serializable Preferences Test`() {
        `Mutable Nullable Serializable Preferences`.test {
            assertEquals(`Mutable Nullable Serializable Preferences`.Defaults.user, user)

            user = User("A", "B")

            assertEquals(User("A", "B"), user)
        }
    }
}
