import com.colorata.superstore.AnonymousPreferenceClassException
import com.colorata.superstore.NoPreferenceConfigException
import com.colorata.superstore.PreferencesScope
import com.colorata.superstore.mutablePreferenceOf
import com.colorata.superstore.superStoreConfig
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class ExceptionsTest {

    @Test
    fun anonymousClassTest() {
        assertThrows<AnonymousPreferenceClassException> {
            val preferences = object : PreferencesScope(
                configuration = superStoreConfig
                    ("/")
            ) {
                var counter by mutablePreferenceOf(1)
            }
            preferences.counter += 1
        }
    }

    object Preferences : PreferencesScope() {
        var counter by mutablePreferenceOf(1)
    }

    @Test
    fun noConfigTest() {
        assertThrows<NoPreferenceConfigException> {
            Preferences.counter += 1
        }
    }
}
