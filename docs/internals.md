# Internals of SuperStore

## Compile-time safety

SuperStore provides compile-time safe preferences. What does it mean?

> Each and every preference is unique. One preference cannot overwrite another.

This means that all possible conflicts are detected in **compile time**.

How does it work?

SuperStore is powered by Kotlin delegates. 
There is an operator functions to handle delegates:
```kotlin
operator fun <T> getValue(
    thisObj: Any?,
    property: KProperty<*>
): T

operator fun <T> setValue(
    thisObj: Any?,
    property: KProperty<*>,
    value: T
)
```

Library gets preference name from `property`. This name is unique in every preference scope(you cannot set 2 identical names in one scope).

Preference store are stored in device memory with full name of `Preference`.
For example:

```kotlin
package com.example.myapp

object Preferences : PreferencesScope()
```
In this case, it will generate file named `com.example.myapp.Preferences.txt`.
This guarantees that one preference store is not writing to another.

<!--Add file contents explanation-->

## File encoding/decoding
By default, all preferences and values are encoded in Base64. For example:
```kotlin
package com.example.myapp
object Preferences: PreferencesScope() {
    var counter by mutablePreferenceOf(0)
}
```
In this case, library will create file named `com.example.myapp.Preferences.txt` with following text:
```text
Y291bnRlcg==:MAo=
```

Text before colon is name of parameter(encoded to Base64), after colon - value of parameter(encoded to Base64 too).

If you want to provide custom encoder/decoder(for example, advanced encrypting/decrypting), use `saver` parameter in `mutablePreferenceOf` or `flowPreferenceOf`:
```kotlin
object Preferences: PreferencesScope() {
    var counter by mutablePreferenceOf(0) { name: String ->
        preferenceSaver(
            save = { value: Int ->
                // this: PreferencesSaver
                // save value to storage
            },
            restore = {
                // this: PreferencesSaver
                // restore value from storage
                return /*restored value*/
            }
        )
    }
}
```
<!--For more details about custom savers, check out [this link](custom-saver.md).-->
## Integration with Compose

SuperStore provides built-in integration with Compose. So, if preference changes, Compose will know about it and will recompose.

In MutablePreference it just simple: every time you change value, internal class just changes internal `mutableState` value:
```kotlin
class MutablePreferenceImpl<T>(
    initial: T
    // ...
) {
    private var _value by mutableStateOf(initial)
    
    operator fun getValue(/**/): T {
        // ...
        return _value
    }
    
    operator fun setValue(/**/ value:T) {
        _value = value
    }
}
```

Kotlin Flows supports built-in integration with Compose, so no work needed.
