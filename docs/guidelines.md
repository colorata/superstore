# Guidelines

## Anonymous objects

✅ **Do**: always create preferences in named objects

```kotlin
object Preferences : PreferencesScope() {
    //...
}
```

❌ **Don't**: don't create preferences in anonymous objects. It breaks compile-time safety of preferences.

```kotlin
val preferences = object : PreferencesScope() {
    //...
}
```

## Defining configuration

✅ **Do**: use platform-specific configuration with platform SDK

```kotlin
class MainActivity : Activity() {
    override fun onCreate() {
        super.onCreate()
        Preferences.config = androidSuperStoreConfig()
    }
}
```

❌ **Don't**: don't hardcode path to preferences directory
```kotlin
class MainActivity: Activity() {
    override fun onCreate() {
        super.onCreate()
        Preferences.config = superStoreConfig("/data/data/com.example.app/files/")
    }
}
```

## Delegates

✅ **Do**: always use delegates to create preferences
```kotlin
object Preferences: PreferencesScope() {
    var counter by mutablePreferenceOf(0)
}
```

❌ **Don't**: don't use delegate's `getValue` and `setValue` because it breaks compile-time safety of preferences
```kotlin
object Preferences: PreferencesScope() {
    val counter = mutablePreferenceOf(0)
}
//...
val c = Preferences.counter.getValue(/**/)
```
