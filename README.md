# SuperStore - compile-time safe preferences

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.gitlab.colorata/superstore/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.gitlab.colorata/superstore/badge.svg)
[![compose-jb-version](https://img.shields.io/badge/compose--jb-1.3.0--beta03-blue)](https://github.com/JetBrains/compose-jb)
![license](https://img.shields.io/gitlab/license/Colorata/Superstore)

![badge-Android](https://img.shields.io/badge/Platform-Android-brightgreen)
![badge-JVM](https://img.shields.io/badge/Platform-JVM-orange)

* Check out [Wiki](https://gitlab.com/colorata/superstore/wikis)

## Setup

Just add library to dependencies in **build.gradle**:
```kotlin
dependencies {
    implementation("com.gitlab.colorata:superstore:2.0.0-alpha04")
}
```

Add `Preference` in your project. It will contain all your preferences:
```kotlin
object Preferences: PreferencesScope() {
    var counterMutable by mutablePreferenceOf(0) // Thread-unsafe
    
    val counterFlow by flowPreferenceOf(0) // Thread-safe
}
```

In Android, configure `Preferences` in OnCreate:
```kotlin
class MainActivity: Activity() {
    override fun onCreate() {
        super.onCreate()
        Preferences.config = androidSuperStoreConfig()
        // Alternatively, you can configure it like this:
        // Preferences.config = superStoreConfig(filesDir.absolutePath)
        //...
    }
}
```

In Jvm, configure `Preferences` in `main` function:
```kotlin
fun main() {
    Preferences.config = superStoreConfig("/myapp/files/")
}
```

You are done! Now you can `Preferences` in your code:
```kotlin
// Mutable Preferences
@Composable
fun SimpleButtonMutable() {
    Button(onClick = { Preferences.counterMutable += 1}) {
        Text("You clicked ${Preferences.counterMutable} times")
    }
}

// Flow Preferences
@Composable
fun SimpleButtonFlow() {
    val scope = rememberCoroutineScope()
    val counter by Preferences.counterFlow.collectAsState(0)
    
    Button(onClick = {
        scope.launch {
            Preferences.counterFlow.emit(counter + 1)
        }
    }) {
        Text("You clicked $counter times")
    }
}
```

## Next steps

* [Guidelines](docs/guidelines.md). How to follow clean code
* [Internals](docs/internals.md). How this library actually works
